#!/bin/bash
set -ex

pkg=stringtie
CUR_DIR=`pwd`

if [ "${AUTOPKGTEST_TMP}" = "" ] ; then
  AUTOPKGTEST_TMP=$(mktemp -d /tmp/${pkg}-test.XXXXXX)
  trap "rm -rf ${AUTOPKGTEST_TMP}" 0 INT QUIT ABRT PIPE TERM
fi

cp ${CUR_DIR}/debian-tests-data/* -a "${AUTOPKGTEST_TMP}"

cd "${AUTOPKGTEST_TMP}"
gunzip -r *

for f in *.sam; do
	outfile=${f::-4}.bam
	cat $f | samtools view -Sb - > $outfile
done

arrins=("short_reads" "short_reads_and_superreads" "long_reads" "long_reads" \
  "mix_short mix_long" "mix_short mix_long")
arrparms=("" "" "-L" "-L -G human-chr19_P.gff" "--mix" "--mix -G mix_guides.gff")
arrout=("short_reads" "short_reads_and_superreads" "long_reads" "long_reads_guided" \
  "mix_reads" "mix_reads_guided")
arrmsg=("Short reads"  "Short reads and super-reads" "Long reads" \
 "Long reads with annotation guides" "Mixed reads" "Mixed reads with annotation guides")
for i in ${!arrmsg[@]}; do
 fout="${arrout[$i]}.out.gtf"
 /bin/rm -f $fout
 fcmp="${arrout[$i]}.out_expected.gtf"
 n=$i
 echo $n
 n=$((n+1))
 echo "Test ${n}: ${arrmsg[$i]}"
 fin=${arrins[$i]}.bam
 if [[ ${arrins[$i]} =~ ^mix ]]; then
   ins=( ${arrins[$i]} )
   fin="${ins[0]}.bam ${ins[1]}.bam"
 fi

 echo ${arrparms[$i]} $fout $fin
 stringtie ${arrparms[$i]} -o $fout $fin
 if [ ! -f $fout ]; then
   echo "Error: file $fout not created! Failed running stringtie on $fin"
   exit 1
 fi
 if diff -q -I '^#' $fout $fcmp &>/dev/null; then
    echo "  OK."
 else
   echo "Error: test failed, output $fout different than expected ($fcmp)!"
   #exit 1
 fi
done
